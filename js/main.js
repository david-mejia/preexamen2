function cargarRazas() {
    fetch('https://dog.ceo/api/breeds/list')
        .then(response => response.json())
        .then(data => {
            const razasSelect = document.getElementById('razas');
            razasSelect.innerHTML = '<option value="">Selecciona una raza</option>';
            data.message.forEach(raza => {
                razasSelect.innerHTML += `<option value="${raza}">${raza}</option>`;
            });
        })
        .catch(error => console.error('Error al cargar las razas:', error));
}

function verImagen() {
    const razaSeleccionada = document.getElementById('razas').value;
    if (razaSeleccionada) {
        fetch(`https://dog.ceo/api/breed/${razaSeleccionada}/images/random`)
            .then(response => response.json())
            .then(data => {
                const dogImage = document.getElementById('dogImage');
                dogImage.src = data.message;
            })
            .catch(error => console.error('Error al cargar la imagen:', error));
    }
}
